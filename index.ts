type Point = {
  x: number,
  y: number,
};

type Line = {//Ax+By+C = 0
  A: number,
  B: number,
  C: number,
};

let isFirstPoint: Boolean = true;


const claculate: HTMLButtonElement = document.querySelector('#claculate');
const cleanButton:HTMLButtonElement = document.querySelector('#clean');
const answer:HTMLElement = document.querySelector('#answer');
const addButton:HTMLElement = document.querySelector('#addPoint');
const poitX: HTMLInputElement = document.querySelector('#pointX');
const poitY: HTMLInputElement = document.querySelector('#pointY');
const canvas: HTMLCanvasElement = document.querySelector('#canvas');
const context: RenderingContext = canvas.getContext("2d");
let arrPoints: Array<Point> = [];
context.beginPath();


function getCursorPosition(canvas: HTMLCanvasElement, event: MouseEvent): Point {
  const rect: DOMRect  = canvas.getBoundingClientRect();
  const x: number = event.clientX - rect.left;
  const y: number = event.clientY - rect.top;
  
  return {
    x: x,
    y: y,
  }
}

function square(points: Array<Point>): number {
  let s: number = 0;

  if (points.length == 3) {
    s = areaTriangle(points[0],points[1],points[2]);
  }

  if (points.length == 4) {
    let intersections: Point = cramer(getLine(points[0],points[1]),getLine(points[2],points[3])) as Point;
    if ( (points[0].x < intersections.x && intersections.x < points[1].x) || (points[1].x < intersections.x && intersections.x < points[0].x)) {
      s = areaTriangle(points[0],points[3],intersections) + areaTriangle(points[1],points[2],intersections);
    } else {
      let d1: number = lengthLine(points[0],points[2]);
      let d2: number = lengthLine(points[1],points[3]);
      let alfa: number = angle(getLine(points[0],points[2]),getLine(points[1],points[3]));
      s = (d1*d2*Math.sin(alfa))/2
    } 
  }

  if (points.length > 4) {
    for(let i = 1; i < points.length - 1 ; i++) {
      s += areaTriangle(points[0],points[i],points[i+1]);
    }
  }

  return s > 0 ? s: -s;
}

function areaTriangle(point1: Point, point2: Point, point3: Point): number {

  return ((point1.x - point3.x)*(point2.y - point3.y) - (point2.x - point3.x)*(point1.y - point3.y))/2;
}

function angle(line1: Line, line2: Line): number {
  let cosA: number = (line1.A*line2.A + line1.B*line2.B)/(Math.sqrt((line1.A**2 + line1.B**2)*(line2.A**2 + line2.B**2)));
  let alfa: number = Math.acos(cosA);
  return alfa;
}

function getLine(pointStart: Point, pointEnd: Point): Line {

  return {
    A: pointStart.y - pointEnd.y,
    B: pointEnd.x - pointStart.x,
    C: pointStart.x*pointEnd.y - pointEnd.x*pointStart.y,
  }
}

function lengthLine(point1: Point,point2: Point): number {
  return  Math.sqrt(Math.pow((point2.x - point1.x),2) + Math.pow((point2.y - point1.y),2));
}

function cramer(line1: Line, line2: Line): Point | string {
  let detA = line1.A*line2.B - line2.A*line1.B; 
  if (detA == 0) {

    return "No intersections "
  }

  return {
    x: (-line1.C*line2.B+line2.C*line1.B)/detA,
    y:(-line2.C*line1.A+line1.C*line2.A)/detA,
  }
}

function handlerClick(point: Point,context): void {
  let {x,y} = point;
  if (isFirstPoint) {
    isFirstPoint = false;
    context.moveTo(x,y);
  }
  arrPoints.push(point);

  context.lineTo(x, y);
  context.strokeStyle = "red";
  context.stroke();
}


canvas.addEventListener('click', function(e: MouseEvent): void {
  let point: Point = getCursorPosition(canvas, e);
  handlerClick(point,context);
});


addButton.addEventListener('click', function(e: MouseEvent): void {
  let point: Point = {
    x: +poitX.value,
    y: +poitY.value,
  };
  handlerClick(point,context);
  poitX.value = '';
  poitY.value = '';

});



claculate.addEventListener('click', function(e: MouseEvent): void {
  context.closePath();
  context.fillStyle = 'red';
  context.fill();
  context.stroke();
  let content = square(arrPoints) == 0? "Sorry, I can't calculate the area of such a figure" :  `S = ${square(arrPoints)} px<sup>2</sup>`;
  answer.innerHTML = content; 

});


cleanButton.addEventListener('click', function(e: MouseEvent): void {
  answer.innerHTML = ``; 
  isFirstPoint = true;
  arrPoints = [];
  canvas.width = canvas.width
  poitX.value = '';
  poitY.value = '';
});

