var isFirstPoint = true;
var claculate = document.querySelector('#claculate');
var cleanButton = document.querySelector('#clean');
var answer = document.querySelector('#answer');
var addButton = document.querySelector('#addPoint');
var poitX = document.querySelector('#pointX');
var poitY = document.querySelector('#pointY');
var canvas = document.querySelector('#canvas');
var context = canvas.getContext("2d");
var arrPoints = [];
context.beginPath();
function getCursorPosition(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return {
        x: x,
        y: y
    };
}
function square(points) {
    var s = 0;
    if (points.length == 3) {
        s = areaTriangle(points[0], points[1], points[2]);
    }
    if (points.length == 4) {
        var intersections = cramer(getLine(points[0], points[1]), getLine(points[2], points[3]));
        if ((points[0].x < intersections.x && intersections.x < points[1].x) || (points[1].x < intersections.x && intersections.x < points[0].x)) {
            s = areaTriangle(points[0], points[3], intersections) + areaTriangle(points[1], points[2], intersections);
        }
        else {
            var d1 = lengthLine(points[0], points[2]);
            var d2 = lengthLine(points[1], points[3]);
            var alfa = angle(getLine(points[0], points[2]), getLine(points[1], points[3]));
            s = (d1 * d2 * Math.sin(alfa)) / 2;
        }
    }
    if (points.length > 4) {
        console.log(points);
        for (var i = 1; i < points.length - 1; i++) {
            s += areaTriangle(points[0], points[i], points[i + 1]);
        }
    }
    return s > 0 ? s : -s;
}
function areaTriangle(point1, point2, point3) {
    return ((point1.x - point3.x) * (point2.y - point3.y) - (point2.x - point3.x) * (point1.y - point3.y)) / 2;
}
function angle(line1, line2) {
    var cosA = (line1.A * line2.A + line1.B * line2.B) / (Math.sqrt((Math.pow(line1.A, 2) + Math.pow(line1.B, 2)) * (Math.pow(line2.A, 2) + Math.pow(line2.B, 2))));
    var alfa = Math.acos(cosA);
    return alfa;
}
function getLine(pointStart, pointEnd) {
    return {
        A: pointStart.y - pointEnd.y,
        B: pointEnd.x - pointStart.x,
        C: pointStart.x * pointEnd.y - pointEnd.x * pointStart.y
    };
}
function lengthLine(point1, point2) {
    return Math.sqrt(Math.pow((point2.x - point1.x), 2) + Math.pow((point2.y - point1.y), 2));
}
function cramer(line1, line2) {
    var detA = line1.A * line2.B - line2.A * line1.B;
    if (detA == 0) {
        return "No intersections ";
    }
    return {
        x: (-line1.C * line2.B + line2.C * line1.B) / detA,
        y: (-line2.C * line1.A + line1.C * line2.A) / detA
    };
}
function handlerClick(point, context) {
    var x = point.x, y = point.y;
    if (isFirstPoint) {
        isFirstPoint = false;
        context.moveTo(x, y);
    }
    arrPoints.push(point);
    context.lineTo(x, y);
    context.strokeStyle = "red";
    context.stroke();
}
canvas.addEventListener('click', function (e) {
    var point = getCursorPosition(canvas, e);
    handlerClick(point, context);
});
addButton.addEventListener('click', function (e) {
    var point = {
        x: +poitX.value,
        y: +poitY.value
    };
    handlerClick(point, context);
    poitX.value = '';
    poitY.value = '';
});
claculate.addEventListener('click', function (e) {
    context.closePath();
    context.fillStyle = 'red';
    context.fill();
    context.stroke();
    var content = square(arrPoints) == 0 ? "Sorry, I can't calculate the area of such a figure" : "S = " + square(arrPoints) + " px<sup>2</sup>";
    answer.innerHTML = content;
});
cleanButton.addEventListener('click', function (e) {
    answer.innerHTML = "";
    isFirstPoint = true;
    arrPoints = [];
    canvas.width = canvas.width;
    poitX.value = '';
    poitY.value = '';
});
